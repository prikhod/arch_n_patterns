import abc
import sqlite3
import threading

from models import User, Student, OnlineCourse, OfflineCourse, Course, Category

connection = sqlite3.connect('patterns.sqlite')


class RecordNotFoundException(Exception):
    def __init__(self, message):
        super().__init__(f'Record not found: {message}')


class DbCommitException(Exception):
    def __init__(self, message):
        super().__init__(f'Db commit error: {message}')


class DbUpdateException(Exception):
    def __init__(self, message):
        super().__init__(f'Db update error: {message}')


class DbDeleteException(Exception):
    def __init__(self, message):
        super().__init__(f'Db delete error: {message}')


class StudentMapper(metaclass=abc.ABC):
    @abc.abstractmethod
    def find_by_id(self, id_person):
        pass

    @abc.abstractmethod
    def insert(self, student):
        pass

    @abc.abstractmethod
    def update(self, student):
        pass

    @abc.abstractmethod
    def delete(self, student):
        pass


class OnlineCourseMapper(metaclass=abc.ABC):
    @abc.abstractmethod
    def find_by_id(self, id_course):
        pass

    @abc.abstractmethod
    def insert(self, course):
        pass

    @abc.abstractmethod
    def update(self, course):
        pass

    @abc.abstractmethod
    def delete(self, course):
        pass


class OfflineCourseMapper(metaclass=abc.ABC):
    @abc.abstractmethod
    def find_by_id(self, id_course):
        pass

    @abc.abstractmethod
    def insert(self, course):
        pass

    @abc.abstractmethod
    def update(self, course):
        pass

    @abc.abstractmethod
    def delete(self, course):
        pass


class CourseMapper(metaclass=abc.ABC):
    @abc.abstractmethod
    def find_by_id(self, id_course):
        pass

    @abc.abstractmethod
    def find_all(self):
        pass

    @abc.abstractmethod
    def count_by_category(self, category_id):
        pass

    @abc.abstractmethod
    def insert(self, course):
        pass

    @abc.abstractmethod
    def update(self, course):
        pass

    @abc.abstractmethod
    def delete(self, course):
        pass


class CategoryMapper(metaclass=abc.ABC):
    @abc.abstractmethod
    def find_by_id(self, id_category):
        pass

    @abc.abstractmethod
    def find_all(self):
        pass

    @abc.abstractmethod
    def insert(self, category):
        pass

    @abc.abstractmethod
    def update(self, category):
        pass

    @abc.abstractmethod
    def delete(self, category):
        pass


class SqliteStudentMapper(StudentMapper):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def find_by_id(self, id_student):
        statement = f"SELECT ID, FIRSTNAME, LASTNAME, EMAIL, PHONE FROM STUDENTS WHERE ID=?"
        self.cursor.execute(statement, (id_student,))
        result = self.cursor.fetchone()
        if result:
            return Student(*result)
        else:
            raise RecordNotFoundException(f'record with id={id_student} not found')

    def insert(self, student):
        statement = f"INSERT INTO PERSON (ID, FIRSTNAME, LASTNAME, EMAIL, PHONE) VALUES (?, ?, ?, ?)"
        self.cursor.execute(statement,
                            (student.firstname,
                             student.lastname,
                             student.email,
                             student.phone))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbCommitException(e.args)

    def update(self, student):
        statement = f"UPDATE STUDENT SET FIRSTNAME=?, LASTNAME=?, EMAIL=?, PHONE=? WHERE ID=?"
        self.cursor.execute(statement, (student.first_name,
                                        student.last_name,
                                        student.email,
                                        student.phone,
                                        student.id))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbUpdateException(e.args)

    def delete(self, student):
        statement = f"DELETE FROM STUDENT WHERE ID=?"
        self.cursor.execute(statement, (student.id,))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbDeleteException(e.args)


class SqliteOnlineCourseMapper(OnlineCourseMapper):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def find_by_id(self, id_course):
        statement = f"SELECT ID, LINK FROM ONLINECOURSE WHERE ID=?"
        self.cursor.execute(statement, (id_course,))
        result = self.cursor.fetchone()
        if result:
            return OnlineCourse(*result)
        else:
            raise RecordNotFoundException(f'record with id={id_course} not found')

    def insert(self, course):
        statement = f"INSERT INTO ONLINECOURSE (ID, LINK) VALUES (?, ?)"
        self.cursor.execute(statement,
                            (course.id,
                             course.link))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbCommitException(e.args)

    def update(self, course):
        statement = f"UPDATE ONLINECOURSE SET ID=?, LINK=? WHERE ID=?"
        self.cursor.execute(statement, (course.id,
                                        course.link))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbUpdateException(e.args)

    def delete(self, course):
        statement = f"DELETE FROM ONLINECOURSE WHERE ID=?"
        self.cursor.execute(statement, (course.id,))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbDeleteException(e.args)


class SqliteOfflineCourseMapper(OfflineCourseMapper):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def find_by_id(self, id_course):
        statement = f"SELECT ID, ADDRESS FROM OFFLINECOURSE WHERE ID=?"
        self.cursor.execute(statement, (id_course,))
        result = self.cursor.fetchone()
        if result:
            return OfflineCourse(*result)
        else:
            raise RecordNotFoundException(f'record with id={id_course} not found')

    def insert(self, course):
        statement = f"INSERT INTO OFFLINECOURSE (ID, ADDRESS) VALUES (?, ?)"
        self.cursor.execute(statement,
                            (course.id,
                             course.address))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbCommitException(e.args)

    def update(self, course):
        statement = f"UPDATE OFFLINECOURSE SET ADDRESS=? WHERE ID=?"
        self.cursor.execute(statement, (course.address,
                                        course.id))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbUpdateException(e.args)

    def delete(self, course):
        statement = f"DELETE FROM OFFLINECOURSE WHERE ID=?"
        self.cursor.execute(statement, (course.id,))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbDeleteException(e.args)


class SqliteCourseMapper(CourseMapper):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def find_by_id(self, id_course):
        statement = f"SELECT ID, NAME, CATEGORY, COURSE_FORMAT FROM COURSE WHERE ID=?"
        self.cursor.execute(statement, (id_course,))
        result = self.cursor.fetchone()
        if result:
            return Course(*result)
        else:
            raise RecordNotFoundException(f'record with id={id_course} not found')

    def find_all(self):
        statement = f"SELECT ID, NAME, CATEGORY, COURSE_FORMAT FROM COURSE"
        self.cursor.execute(statement)
        result = self.cursor.fetchall()
        if result:
            return [Course(*course) for course in result]
        else:
            raise RecordNotFoundException(f'records not found')

    def count_by_category(self, category_id):
        statement = f"SELECT COUNT(*) FROM COURSE WHERE CATEGORY=?"
        self.cursor.execute(statement, (category_id,))
        result = self.cursor.fetchone()
        if result:
            return result.value()
        else:
            raise RecordNotFoundException(f'records not found')

    def insert(self, course):
        statement = f"INSERT INTO COURSE (ID, NAME, CATEGORY, COURSE_FORMAT) VALUES (?, ?)"
        self.cursor.execute(statement,
                            (course.id,
                             course.name,
                             course.category,
                             course.course_format))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbCommitException(e.args)

    def update(self, course):
        statement = f"UPDATE COURSE SET NAME=?, CATEGORY=?, COURSE_FORMAT=? WHERE ID=?"
        self.cursor.execute(statement, (course.name,
                                        course.category,
                                        course.course_format,
                                        course.id))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbUpdateException(e.args)

    def delete(self, course):
        statement = f"DELETE FROM COURSE WHERE ID=?"
        self.cursor.execute(statement, (course.id,))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbDeleteException(e.args)


class SqliteCategoryMapper(CategoryMapper):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def find_by_id(self, id_category):
        statement = f"SELECT ID, NAME, PARENT FROM CATEGORY WHERE ID=?"
        self.cursor.execute(statement, (id_category,))
        result = self.cursor.fetchone()
        if result:
            return Category(*result)
        else:
            raise RecordNotFoundException(f'record with id={id_category} not found')

    def find_all(self):
        statement = f"SELECT ID, NAME, PARENT FROM CATEGORY"
        self.cursor.execute(statement)
        result = self.cursor.fetchall()
        if result:
            return [Category(*category) for category in result]
        else:
            raise RecordNotFoundException(f'records not found')

    def insert(self, category):
        statement = f"INSERT INTO CATEGORY (ID, NAME, PARENT) VALUES (?, ?)"
        self.cursor.execute(statement,
                            (category.id,
                             category.name,
                             category.parent))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbCommitException(e.args)

    def update(self, category):
        statement = f"UPDATE CATEGORY SET NAME=?, CATEGORY=?, COURSE_FORMAT=? WHERE ID=?"
        self.cursor.execute(statement, (category.name,
                                        category.parent,
                                        category.id))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbUpdateException(e.args)

    def delete(self, category):
        statement = f"DELETE FROM CATEGORY WHERE ID=?"
        self.cursor.execute(statement, (category.id,))
        try:
            self.connection.commit()
        except Exception as e:
            raise DbDeleteException(e.args)


class MapperRegistry:
    @staticmethod
    def get_mapper(obj):
        if isinstance(obj, Student) or obj is Student:
            return SqliteStudentMapper(connection)
        elif isinstance(obj, OnlineCourse) or obj is OnlineCourse:
            return SqliteOnlineCourseMapper(connection)
        elif isinstance(obj, OfflineCourse) or obj is OfflineCourse:
            return SqliteOfflineCourseMapper(connection)
        elif isinstance(obj, Course) or obj is Course:
            return SqliteCourseMapper(connection)
        elif isinstance(obj, Category) or obj is Category:
            return SqliteCategoryMapper(connection)


class UnitOfWork:
    current = threading.local()

    def __init__(self):
        self.new_objects = []
        self.dirty_objects = []
        self.removed_objects = []

    def register_new(self, obj):
        self.new_objects.append(obj)

    def register_dirty(self, obj):
        self.dirty_objects.append(obj)

    def register_removed(self, obj):
        self.removed_objects.append(obj)

    def commit(self):
        self.insert_new()
        self.update_dirty()
        self.delete_removed()

    def insert_new(self):
        for obj in self.new_objects:
            MapperRegistry.get_mapper(obj).insert(obj)

    def update_dirty(self):
        for obj in self.dirty_objects:
            MapperRegistry.get_mapper(obj).update(obj)

    def delete_removed(self):
        for obj in self.removed_objects:
            MapperRegistry.get_mapper(obj).delete(obj)

    @staticmethod
    def new_current():
        __class__.set_current(UnitOfWork())

    @classmethod
    def set_current(cls, unit_of_work):
        cls.current.unit_of_work = unit_of_work

    @classmethod
    def get_current(cls):
        return cls.current.unit_of_work


class DomainObject(metaclass=abc.ABC):
    def mark_new(self):
        UnitOfWork.get_current().register_new(self)

    def mark_dirty(self):
        UnitOfWork.get_current().register_dirty(self)

    def mark_removed(self):
        UnitOfWork.get_current().register_removed(self)
