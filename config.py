import yaml


class Config:
    def __init__(self, data: dict):
        self.templates = data['templates']
        self.debug = data['debug']


def parse(filepath):
    with open(filepath, "r") as f:
        cfg = yaml.load(f, Loader=yaml.SafeLoader)
    return cfg
