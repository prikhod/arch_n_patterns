from forelock.renderer import Render
import traceback


class NotFoundPage:
    def __call__(self, request):
        return '404 Not Found', Render.render_error(data={'msg': '404 Not Found', 'detail': ''})


class PageController:
    def __init__(self, controller, debug):
        self._controller = controller
        self._debug = debug

    def __call__(self, request):
        try:
            return '200 OK', self._controller(request)
        except Exception as e:
            if self._debug:
                msg = '500 Internal Server Error'
                return msg, Render.render_error(data={'msg': msg, 'detail': traceback.format_exc()})
            return '500 Internal Server Error', b'500 Internal Server Error'
