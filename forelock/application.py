from forelock.controller import NotFoundPage, PageController
from forelock.front_controller import FrontController


class Application:
    def __init__(self, routes, fronts, cfg):
        self._routes = routes
        self._debug = cfg.debug
        self._fronts = fronts

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO']
        method = environ['REQUEST_METHOD']
        data = self.get_wsgi_input_data(environ)
        data = self.parse_wsgi_input_data(data)
        data.update(self._parse_input_data(environ['QUERY_STRING']))
        request = {}
        if path in self._routes:
            controller = PageController(self._routes[path], self._debug)
            request['data'] = data
            request['method'] = method
            for front in self._fronts:
                FrontController(front)(request)
        else:
            controller = NotFoundPage()

        code, body = controller(request)
        start_response(code, [('Content-Type', 'text/html')])
        return [body]

    def _parse_input_data(self, data: str):
        result = {}
        if data:
            params = data.split('&')
            for item in params:
                k, v = item.split('=')
                result[k] = v
        return result

    def get_wsgi_input_data(self, env) -> bytes:
        content_length_data = env.get('CONTENT_LENGTH')
        content_length = int(content_length_data) if content_length_data else 0
        data = env['wsgi.input'].read(content_length) if content_length > 0 else b''
        return data

    def parse_wsgi_input_data(self, data: bytes) -> dict:
        result = {}
        if data:
            data_str = data.decode(encoding='utf-8')
            result = self._parse_input_data(data_str)
        return result



