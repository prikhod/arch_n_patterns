import copy


class BadFrontControllerError(Exception):
    pass


class FrontController:
    def __init__(self, controller):
        self._controller = controller

    def __call__(self, request):
        _request = copy.deepcopy(request)
        self._controller(request)
        if len(_request) >= len(request):
            raise BadFrontControllerError
