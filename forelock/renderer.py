from jinja2 import Template, TemplateNotFound, FileSystemLoader, Environment


class Render:
    template_path = None
    forelock_template_path = 'forelock/templates/'

    @classmethod
    def render(cls, template_name, data):
        templateLoader = FileSystemLoader(searchpath=cls.template_path)
        templateEnv = Environment(loader=templateLoader)
        template = templateEnv.get_template(template_name)
        return template.render(data=data).encode()

    @classmethod
    def render_error(cls, data):
        templateLoader = FileSystemLoader(searchpath=cls.forelock_template_path)
        templateEnv = Environment(loader=templateLoader)
        TEMPLATE_FILE = "error.html"
        template = templateEnv.get_template(TEMPLATE_FILE)
        return template.render(data=data).encode()
