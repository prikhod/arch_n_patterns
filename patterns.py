from abc import ABC, abstractmethod


class Observer(ABC):
    def __init__(self, subscriber_name):
        self._subject = None
        self._observer_state = None
        self._subscriber_name = subscriber_name

    @abstractmethod
    def update(self, arg):
        pass


class Observable:
    def __init__(self):
        self._observers = set()

    def subscribe(self, observer):
        observer._subject = self
        self._observers.add(observer)

    def unsubscribe(self, observer):
        observer._subject = None
        self._observers.discard(observer)

    def notify_observers(self, message):
        for observer in self._observers:
            observer.update(message)

