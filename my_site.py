import argparse

from config import Config, parse
from forelock import Application
from forelock.renderer import Render

from front_controllers import secret_front, other_front
from page_controllers import index_page, about_page, page_with_internal_error, contacts, feedback, create_course, \
    create_category, courses, edit_course, create_student

#  SETTINGS
""" Hard Code: необходимо считывать директорию с темплейтам, например, из конфига"""

routes = {
    '/': index_page,
    '/about': about_page,
    '/page_with_internal_error': page_with_internal_error,
    '/contacts': contacts,
    '/feedback_form': feedback,
    '/create_category': create_category,
    '/create_course': create_course,
    '/courses': courses,
    '/edit_course': edit_course,
    '/create_student': create_student,
}
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="cfg file", required=True)
    args = parser.parse_args()
    cfg = Config(parse(args.config))
    Render.template_path = cfg.templates

    app = Application(routes, [secret_front, other_front], cfg)
