import datetime


def debug(func):
    def wrapper(*args, **kwargs):
        print(f'{datetime.datetime.now().strftime("%d/%b/%Y:%H:%M:%S")} func: "{func.__name__}()"')
        return func(*args, **kwargs)
    return wrapper
