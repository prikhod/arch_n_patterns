import sqlite3
import uuid
from abc import ABC

from mapper import DomainObject, UnitOfWork, MapperRegistry
from patterns import Observer, Observable


class User(ABC):
    def __init__(self, firstname, lastname, email, phone, course, notifiers):
        self._firstname = firstname
        self._lastname = lastname
        self._email = email
        self._phone = phone
        self._courses = set()
        self._notifiers = notifiers
        self.id = str(uuid.uuid4())
        for notifier in self._notifiers:
            course.subscribe(notifier(self._firstname))
        self._courses.add(course)

    """Предполагаю, что на курс может быть записан как студент, так и преподаватель"""
    def add_course(self, course):
        self._courses.add(course)
        for notifier in self._notifiers:
            course.subscribe(notifier(self._firstname))

    def delete_from_course(self, course):
        self._courses.remove(course)
        for notifier in self._notifiers:
            course.unsubscribe(notifier)


class Student(User, DomainObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Category(DomainObject):
    def __init__(self, name, parent=None):
        self._name = name
        self._parent = parent
        self.id = str(uuid.uuid4())

    @property
    def name_tree(self):
        if self._parent:
            return f'{self._parent.parents}->{self._name}'
        return self._name

    def __repr__(self):
        if self._parent:
            return f'{self._parent.parents}->{self._name}'
        return self._name

    @property
    def parents(self):
        if self._parent:
            return self.name_tree
        return self._name

    def __lt__(self, other):  # for sorted
        return self.name_tree < other.name_tree


class CourseFormat(ABC):
    pass


class OnlineCourse(CourseFormat, DomainObject):
    def __init__(self, link):
        self.id = str(uuid.uuid4())
        self._link = link

    def __repr__(self):
        return f'link: {self._link}'


class OfflineCourse(CourseFormat, DomainObject):
    def __init__(self, address):
        self.id = str(uuid.uuid4())
        self._address = address

    def __repr__(self):
        return f'address: {self._address}'


class CourseFactory:
    course_types = {'online': OfflineCourse,
                    'offline': OfflineCourse}

    @classmethod
    def get_course_by_type(cls, _type):
        return cls.course_types[_type]


class Notifier(Observer):

    def __init__(self, subscriber_name, _type, instructions):
        super().__init__(subscriber_name)
        self.subscriber_id = subscriber_name
        self.type = _type
        self.instructions = instructions

    def update(self, arg):
        print(f'{self._subscriber_name}: {self.type} notify: {arg}')


class Course(Observable, DomainObject):
    def __init__(self, name, category, course_format):
        super().__init__()
        self._name = name
        self._category = category
        self._course_format = course_format
        self.id = str(uuid.uuid4())

    @property
    def info(self):
        return f'{self._name} {self._course_format}'

    @property
    def name(self):
        return self._name

    @property
    def course_format(self):
        return self._course_format

    @property
    def category(self):
        return self._category

    def edit(self, **kwargs):
        edited_fields = []
        """ чтобы не проверять каждый атрибут вручную: """
        for field, value in kwargs.items():
            field = f'_{field}'
            if field not in self.__dict__ and not isinstance(value, self.__dict__[field]):
                raise KeyError(f'Course has not attribute {field.strip("_")}')
            self.__dict__[field] = value
            edited_fields.append(f'Course change {field.strip("_")}, new value: {value}')
        self.notify_observers('\n'.join(edited_fields))


class DB:
    connection = sqlite3.connect('school.sqlite')

    @classmethod
    def create_category(cls, name, parent_id=None):
        UnitOfWork.new_current()
        parent = cls.get_category_by_id(parent_id)
        category = Category(name, parent)
        category.mark_new()
        mapper = MapperRegistry.get_mapper(category)(cls.connection)
        mapper.insert(category)
        UnitOfWork.get_current().commit()

    @classmethod
    def get_categories(cls):
        mapper = MapperRegistry.get_mapper(Category)(cls.connection)
        categories = mapper.find_all()
        return sorted([(category, cls.get_count_courses_by_category(category)) for category in categories],
                      key=lambda x: x[0])

    @classmethod
    def create_course(cls, name, _type, category_id, place):
        course_format = CourseFactory.get_course_by_type(_type)(place)
        category = cls.get_category_by_id(category_id)
        UnitOfWork.new_current()
        course = Course(name, category.id, course_format)
        course.mark_new()
        mapper = MapperRegistry.get_mapper(course)(cls.connection)
        mapper.insert(course)
        UnitOfWork.get_current().commit()

    @classmethod
    def get_courses(cls):
        mapper = MapperRegistry.get_mapper(Course)(cls.connection)
        return mapper.find_all()

    @classmethod
    def get_count_courses_by_category(cls, id_category):
        mapper = MapperRegistry.get_mapper(Course)(cls.connection)
        return mapper.count_by_category(id_category)

    @classmethod
    def get_category_by_id(cls, category_id):
        mapper = MapperRegistry.get_mapper(Category)(cls.connection)
        return mapper.find_by_id(category_id)

    @classmethod
    def get_course_by_id(cls, course_id) -> Course:
        mapper = MapperRegistry.get_mapper(Course)(cls.connection)
        return mapper.find_by_id(course_id)

    @classmethod
    def create_student(cls, firstname, lastname, phone, email, course_id):
        UnitOfWork.new_current()
        """ Blind faith: необходимо проверять существует ли курс с course_id"""
        course = cls.get_course_by_id(course_id)
        student = Student(firstname, lastname, phone, email, course.id)
        student.mark_new()
        mapper = MapperRegistry.get_mapper(Student)(cls.connection)
        mapper.insert(student)
        UnitOfWork.get_current().commit()
