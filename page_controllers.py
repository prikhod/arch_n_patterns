from pprint import pprint

from decorators import debug
from forelock.renderer import Render
from models import DB, CourseFactory


def index_page(request):
    data = {'name': 'alex',
            'welcome_text': 'hello'}
    return Render.render('index_page.html', data)


def feedback(request):
    if request['method'] == 'POST':
        subject = request['data']['subject']
        body = request['data']['body']
        email = request['data']['email']
        pprint(request['data'])
        data = {'name': email,
                'welcome_text': 'hello'}
        return Render.render('index_page.html', data)
    return Render.render('contacts.html', {})


def create_student(request):
    data = {'courses': DB.get_courses()}
    if request['method'] == 'POST':
        firstname = request['data']['firstname']
        lastname = request['data']['lastname']
        phone = request['data']['phone']
        email = request['data']['email']
        course_id = request['data']['course'].strip('+++')
        DB.create_student(firstname, lastname, phone, email, course_id)
        return Render.render('create_student.html', data)
    return Render.render('create_student.html', data)


def create_course(request):
    data = {'category': DB.get_categories(),
            'types': CourseFactory.course_types.keys()}
    if request['method'] == 'POST':
        name = request['data']['name']
        category_id = request['data']['category'].strip('+++')
        _type = request['data']['type']
        additional_data = request['data']['additional_data']
        DB.create_course(name, _type, category_id, additional_data)
        return Render.render('create_course.html', data)
    return Render.render('create_course.html', data)


def edit_course(request):
    if request['method'] == 'POST':
        name = request['data']['name']
        category_id = request['data']['category']
        course_id = request['data']['course_id']
        _type = request['data']['type']
        additional_data = request['data']['additional_data']
        course_format = CourseFactory.get_course_by_type(_type)(additional_data)
        DB.get_course_by_id(course_id).edit(name=name,
                                            category=DB.get_category_by_id(category_id),
                                            course_format=course_format)
        data = {'category': DB.get_categories(),
                'types': CourseFactory.course_types.keys(),
                'name': name,
                'cat': category_id,
                'place': str(additional_data)}
        return Render.render('edit_course.html', data)
    course = DB.get_course_by_id(request['data']['course_id'])
    data = {'category': DB.get_categories(),
            'types': CourseFactory.course_types.keys(),
            'name': course.name,
            'course_id': request['data']['course_id'],
            'cat': course.category.id,
            'place': str(course.course_format)}
    return Render.render('edit_course.html', data)


def create_category(request):
    if request['method'] == 'POST':
        name = request['data']['name']
        parent_id = request['data']['parent_id']
        DB.create_category(name, parent_id)
        data = {'category': DB.get_categories()}
        return Render.render('create_category.html', data)
    data = {'category': DB.get_categories()}
    return Render.render('create_category.html', data)


def courses(request):
    data = {'courses': DB.get_courses()}
    return Render.render('courses.html', data)


def contacts(request):
    data = {'name': 'alex',
            'welcome_text': 'hello'}
    return Render.render('contacts.html', data)


@debug
def about_page(request):
    data = {'site_name': 'my test site',
            'phone': '+7777777777'}
    return Render.render('about_page.html', data)


def page_with_internal_error(request):
    data = {'site_name': 'my test site',
            'phone': 'test'}.encode()  # error
    return Render.render('about_page.html', data)
